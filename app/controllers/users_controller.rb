class UsersController < ApplicationController
  def index
    @users = User.all
    render plan: @users.pluck(:fullname, :email)
  end
end
